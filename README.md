# [**Heroku**](https://price-spy-v02.herokuapp.com/)

#### **Data source [Cenoteka.rs](https://cenoteka.rs/)**.

#### **Data were collected(scraped) and uploaded to Firebase using Python and BeautifulSoup4 library.**


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
