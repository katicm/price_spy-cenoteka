import React, { useState, createContext } from "react";
import NavigationBar from "./components/DropdownMenu/NavigationBar";
import MainContent from "./components/DataTable/MainContent";
import LastUpdate from "./components/DataTable/LastUpdate";
import "./App.scss";

export const CategoryContext = createContext(null);

const App = () => {
  const [category, setCategory] = useState("Biljni napici");
  return (
    <CategoryContext.Provider value={{ category, setCategory }}>
      <div className="App">
        <NavigationBar />
        <div className="main_content">
          <MainContent />
        </div>
        <LastUpdate />
      </div>
    </CategoryContext.Provider>
  );
};
export default App;
