export const categoryNames = [
  {
    name: "Namirnice",
    data: [
      "Brašno",
      "Dodaci za jela",
      "Dodaci za konzervisanje",
      "Gotova jela",
      "Jaja",
      "Kečap i paradajz sos",
      "Kvasac",
      "Majonez&senf&ren",
      "Margarin",
      "Mešavina začina",
      "Pirinač",
      "Puding i šlag",
      "Šećer",
      "Sirće",
      "So",
      "Supe i čorbe",
      "Sve za torte i kolače",
      "Testenine",
      "Ulje",
      "Začini"
    ],
    image: "namirnice"
  },
  {
    name: "Zdrava hrana",
    data: [
      "Bezglutenski proizvodi",
      "Biljni napici",
      "Galete",
      "Med",
      "Ovsene kaše",
      "Pahuljice i musli",
      "Šumeće tablete",
      "Suvo i koštunjavo voće",
      "Zaslađivači",
      "Zdravi Namazi"
    ],
    image: "zdrava-hrana"
  },
  {
    name: "Mlečni proizvodi",
    data: [
      "Grčki jogurt",
      "Jogurt",
      "Kiselo mleko",
      "Maslac",
      "Mlečni deserti",
      "Mlečni napici",
      "Mleko",
      "Pavlaka",
      "Sirevi",
      "Sirni namazi",
      "Surutka",
      "Topljeni sirevi",
      "Voćni jogurt"
    ],
    image: "mlecni-proizvodi"
  },
  {
    name: "Voće i povrće",
    data: ["Konzervisano povrće", "Konzervisano voće", "Povrće", "Voće"],
    image: "voce-i-povrce"
  },
  {
    name: "Meso i riba",
    data: [
      "Kobasice",
      "Konzervisani proizvodi",
      "Namazi",
      "Paštete",
      "Pečenica&pršuta&slanina",
      "Salama",
      "Slajs pakovanja",
      "Sveža riba",
      "Sveže meso",
      "Viršle"
    ],
    image: "meso-i-riba"
  },
  {
    name: "Smrznuto",
    data: [
      "Gotovi sladoledi",
      "Riba i plodovi mora",
      "Smrznuto povrće",
      "Smrznuto testo i pecivo",
      "Smrznuto voće"
    ],
    image: "smrznuto"
  },
  {
    name: "Pića",
    data: [
      "Čajevi",
      "Energetski napici",
      "Gazirani sokovi",
      "Kafa",
      "Kvas",
      "Ledeni čajevi",
      "Pivo",
      "Sajder",
      "Sokovi",
      "Vino",
      "Vitaminski napici",
      "Voda",
      "Žestoka alkoholna pića"
    ],
    image: "pica"
  },
  {
    name: "Slatkiši i grickalice",
    data: [
      "Bombone",
      "Bombonjere",
      "Čokolade",
      "Čokoladice i štanglice",
      "Deserti",
      "Dodaci za mleko",
      "Grickalice",
      "Keks&vafl&biskvit",
      "Kremovi",
      "Kroasani",
      "Napolitanke",
      "Rolati",
      "Žvake"
    ],
    image: "slatkisi-i-grickalice"
  },
  {
    name: "Lična higijena",
    data: [
      "Brijači",
      "Čišćenje lica",
      "Dezodoransi",
      "Farbe za kosu",
      "Gelovi za tuširanje",
      "Higijena za žene",
      "Labela",
      "Nega kose",
      "Nega lica",
      "Nega ruku",
      "Nega stopala",
      "Nega tela",
      "Oralna higijena",
      "Papirna konfekcija",
      "Preparati za sunčanje",
      "Prezervativi",
      "Sapuni",
      "Sredstva za brijanje",
      "Štapići za uši",
      "Stik i roll-on"
    ],
    image: "licna-higijena"
  },
  {
    name: "Kućna hemija",
    data: [
      "Deterdžent za posuđe",
      "Dopunska nega veša",
      "Insekticidi",
      "Kapsule za pranje veša",
      "Maramice za pranje veša",
      "Mašinsko pranje posuđa",
      "Omekšivač za veš",
      "Osveživači prostora",
      "Praškasti deterdženti za veš",
      "Sredstva za čišćenje",
      "Tečni deterdženti za veš"
    ],
    image: "kucna-hemija"
  },
  {
    name: "Kutak za bebe",
    data: [
      "Dečija higijena",
      "Dečija hrana",
      "Kašice za decu",
      "Napici",
      "Pelene",
      "Zamensko mleko za decu"
    ],
    image: "kutak-za-bebe"
  },
  {
    name: "Kućni ljubimci",
    data: ["Hrana za mačke", "Hrana za pse"],
    image: "kucni-ljubimci"
  }
];
