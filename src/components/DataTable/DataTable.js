import React from "react";
import ShopsTab from "./ShopsTab";
import PriceTab from "./PriceTab";
import Subcategory from "./Subcategory";
import ImageZoom from "react-medium-image-zoom";
import no from "../../images/no-image.jpg";

const DataTable = ({ data }) => {
  return (
    <div>
      <div>
        <div className="shops">
          <div className="shops_empty" />
          <ShopsTab />
        </div>
        {data.map((item, i) => (
          <React.Fragment key={i}>
            <Subcategory data={data} i={i}></Subcategory>
            <div className="table_column">
              <div className="article">
                <ImageZoom
                  image={{
                    src: `https://cenoteka.rs/${item.image}`,
                    alt: no,
                    className: "article_image"
                  }}
                  zoomImage={{
                    src: `https://cenoteka.rs/${item.image.replace(
                      "medium",
                      "large"
                    )}`,
                    alt: no
                  }}
                  zoomMargin="100"
                />
                <div className="article_name">{item.name}</div>
                <div className="article_mass">{item.mass}</div>
              </div>
              <PriceTab prices={item.prices} />
            </div>
            <div className="shop_price shop_images">
              <ShopsTab />
            </div>
            <div className="shop_price">
              <PriceTab prices={item.prices} />
            </div>
          </React.Fragment>
        ))}
      </div>
    </div>
  );
};
export default DataTable;
