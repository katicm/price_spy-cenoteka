import React, { useState, useEffect } from "react";
import axios from "axios";

const LastUpdate = () => {
  const [date, setDate] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios.get(
        "https://cenoteka-api.firebaseio.com/last_update.json"
      );
      setDate(Object.values(res.data)[0]);
    };
    fetchData();
  }, []);

  return (
    <div className="last_update">
      Poslednji put ažurirano:
      <br /> {date}
    </div>
  );
};
export default LastUpdate;
