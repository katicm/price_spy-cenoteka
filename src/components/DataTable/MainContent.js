import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import DataTable from "./DataTable";
import Pagination from "./Pagination";
import { CategoryContext } from "../../App";

const MainContent = () => {
  const { category } = useContext(CategoryContext);
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(15);

  const lastItem = currentPage * itemsPerPage;
  const firstItem = lastItem - itemsPerPage;
  const currentItems = data.slice(firstItem, lastItem);
  const pageCount = Math.ceil(data.length / itemsPerPage);

  const handlePage = newCurrentPage => {
    if (newCurrentPage <= pageCount && newCurrentPage >= 1) {
      setCurrentPage(newCurrentPage);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(`https://cenoteka-api.firebaseio.com/${category}.json`)
        .then(result => {
          const items = [];
          for (const cat in result.data) {
            for (const sub in result.data[cat]) {
              for (const item in result.data[cat][sub]) {
                items.push(result.data[cat][sub][item]);
              }
            }
          }
          setData(items);
        });
    };
    fetchData();
    setCurrentPage(1);
  }, [category]);

  return (
    <div>
      <DataTable data={currentItems} />
      <Pagination
        pageCount={pageCount}
        currentPage={currentPage}
        handlePage={handlePage}
      />
    </div>
  );
};
export default MainContent;
