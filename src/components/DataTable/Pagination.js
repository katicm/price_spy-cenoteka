import React from "react";

const active = {
  backgroundColor: "#4b96b5",
  color: "white"
};

const Pagination = ({ pageCount, currentPage, handlePage }) => {
  const pageNumbers = [];

  for (let i = 1; i <= pageCount; i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="pag_container">
      <div className="pagination">
        <div onClick={() => handlePage(1)}>First</div>
        <div onClick={() => handlePage(currentPage - 1)}>&#60;&#60;</div>
        {pageNumbers.map(num => (
          <div
            style={currentPage === num ? active : null}
            key={num}
            onClick={() => handlePage(num)}
          >
            {num}
          </div>
        ))}
        <div onClick={() => handlePage(currentPage + 1)}>>></div>
        <div onClick={() => handlePage(pageCount)}>Last</div>
      </div>
    </div>
  );
};
export default Pagination;
