import React from "react";

const PriceTab = ({ prices }) => {
  return (
    <React.Fragment>
      {prices.map((price, i) => (
        <p key={i}>{price}</p>
      ))}
    </React.Fragment>
  );
};
export default PriceTab;
