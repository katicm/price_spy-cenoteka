import React from "react";
import idea from "../../images/idea.png";
import gomex from "../../images/gomex.png";
import maxi from "../../images/maxi.png";
import univer from "../../images/univer.png";
import tempo from "../../images/tempo.png";
import dis from "../../images/dis.png";
import roda from "../../images/roda.png";

const ShopsTab = () => {
  return (
    <React.Fragment>
      <img src={idea} alt="I"></img>
      <img src={gomex} alt="I"></img>
      <img src={maxi} alt="I"></img>
      <img src={univer} alt="I"></img>
      <img src={tempo} alt="I"></img>
      <img src={dis} alt="I"></img>
      <img src={roda} alt="I"></img>
    </React.Fragment>
  );
};
export default ShopsTab;
