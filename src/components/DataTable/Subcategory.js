import React from "react";

const Subcategory = ({ data, i }) => {
  if (i === 0) {
    return <div className="subcategory">{data[i].subcategory}</div>;
  }
  if (data[i].subcategory === data[i - 1].subcategory) {
    return <div />;
  } else {
    return <div className="subcategory">{data[i].subcategory}</div>;
  }
};
export default Subcategory;
