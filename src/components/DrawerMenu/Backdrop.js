import React from "react";

const Backdrop = props => {
  return <div onClick={props.handleClose} className="backdrop"></div>;
};
export default Backdrop;
