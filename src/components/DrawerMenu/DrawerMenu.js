import React, { useContext } from "react";
import { categoryNames } from "../../categoryList";
import { CategoryContext } from "../../App";

const DrawerMenu = ({ subDrawer, setSubDrawer, isOpen, handleClose }) => {
  const { setCategory } = useContext(CategoryContext);

  let drawerCSS = "drawer_menu";

  if (isOpen) {
    drawerCSS = "drawer_menu open";
  }

  const handleClick = item => {
    setCategory(item);
    handleClose();
  };

  return (
    <React.Fragment>
      <div className={drawerCSS}>
        {categoryNames.map(item => (
          <div
            onClick={() => {
              setSubDrawer(item.data);
            }}
            key={item.image}
          >
            <img src={require(`../../images/${item.image}.png`)} alt="X" />
            <p>{item.name}</p>
          </div>
        ))}
      </div>
      {subDrawer.length !== 0 && (
        <div className={drawerCSS}>
          {subDrawer.map(item => (
            <div
              onClick={() => handleClick(item)}
              className="sub_drawer"
              key={item}
            >
              {item}
            </div>
          ))}
          <div
            className="sub_drawer"
            onClick={() => {
              setSubDrawer([]);
            }}
          >
            Vrati se nazad
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
export default DrawerMenu;
