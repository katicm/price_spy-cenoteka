import React, { useContext } from "react";
import { Dropdown } from "semantic-ui-react";
import { CategoryContext } from "../../App";

const DropdownItem = props => {
  const { setCategory } = useContext(CategoryContext);
  const subCategory = [...props.category.data];
  const lSubCategory = subCategory.splice(0, Math.ceil(subCategory.length / 2));

  return (
    <Dropdown.Menu direction={props.direction}>
      <Dropdown.Header
        style={{
          fontSize: "25px",
          color: "#4B96B5",
          paddingLeft: "20px",
          marginBottom: "0px",
          marginTop: "10px"
        }}
        content={props.category.name}
      />
      <Dropdown.Divider
        style={{
          borderTop: "3px solid #4B96B5",
          margin: "8px 20px 3px 20px"
        }}
      />
      <div className="dropdown_menu">
        <div>
          {lSubCategory.map(item => (
            <Dropdown.Item
              onClick={() => {
                setCategory(item);
              }}
              key={item}
              className="dropdown_menu_item"
              text={item}
            />
          ))}
        </div>
        <div>
          {subCategory.map(item => (
            <Dropdown.Item
              onClick={() => {
                setCategory(item);
              }}
              key={item}
              className="dropdown_menu_item"
              text={item}
            />
          ))}
        </div>
      </div>
    </Dropdown.Menu>
  );
};
export default DropdownItem;
