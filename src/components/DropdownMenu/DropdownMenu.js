import React, { useState } from "react";
import { categoryNames } from "../../categoryList";
import { Menu, Dropdown } from "semantic-ui-react";
import DropdownItem from "./DropdownItem";

const DropdownMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpen = () => {
    setIsOpen(false);
  };

  return (
    <Menu style={{ border: "0", height: "100px" }}>
      {categoryNames.map((item, i) => (
        <Dropdown
          key={item.name}
          trigger={
            <div className="trigger">
              <img src={require(`../../images/${item.image}.png`)} alt="X" />
              <p>{item.name}</p>
            </div>
          }
          style={{
            padding: "0px"
          }}
          icon={null}
          item
          simple
          open={isOpen}
          onClick={handleOpen}
        >
          <DropdownItem direction={i > 5 ? "left" : "right"} category={item} />
        </Dropdown>
      ))}
    </Menu>
  );
};
export default DropdownMenu;
