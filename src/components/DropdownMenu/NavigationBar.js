import React, { useState } from "react";
import logo from "../../images/logo_gb.svg";
import hamburger from "../../images/hamburger.png";
import DropdownMenu from "./DropdownMenu";
import DrawerMenu from "../DrawerMenu/DrawerMenu";
import Backdrop from "../DrawerMenu/Backdrop";

const NavigationBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [subDrawer, setSubDrawer] = useState([]);

  const handleClick = () => {
    setIsOpen(prevState => !prevState);
    setSubDrawer([]);
  };
  const handleClose = () => {
    setIsOpen(false);
    setSubDrawer([]);
  };

  return (
    <header className="toolbar">
      <nav className="nav">
        <div>
          <img className="nav_logo" src={logo} alt="X" />
        </div>
        <div className="nav_divider"></div>
        <div className="dropdown">
          <DropdownMenu />
        </div>
        <div className="nav_divider2"></div>
        <div className="nav_hamburger" onClick={handleClick}>
          <img src={hamburger} alt="X"></img>
        </div>
        {isOpen && <Backdrop handleClose={handleClose} />}
        <DrawerMenu
          subDrawer={subDrawer}
          setSubDrawer={setSubDrawer}
          isOpen={isOpen}
          handleClose={handleClose}
        />
      </nav>
    </header>
  );
};
export default NavigationBar;
